(ns adventofcode.day3
  (:require [clojure.string :as string]))

(def input (slurp "src/adventofcode/day3.input"))

(def space-empty? (partial = \.))

(def space-tree? (partial = \#))

(def pattern (string/split input #"\n"))

(def width (-> pattern first count))

(def height (-> pattern count))

(defn next-pos
  [x y]
  [(mod (+ x 3) width) (inc y)])

(defn has-tree?
  [x y]
  (-> pattern (get y) (get x) space-tree?))

;; tree count
(def part-1
  (reduce
    (fn [[[x y] tree-count] _]
      [(next-pos x y)
       (if (has-tree? x y)
         (inc tree-count)
         tree-count)])
    [[0 0] 0]
    (range height)))

(defn move-from-slope
  "returns a function to move according to the slope"
  [sx sy]
  (fn [x y]
    [(mod (+ x sx) width) (+ y sy)]))

(def part-2
  (apply *
         (for [slope [[1 1] [5 1] [3 1] [7 1] [1 2]]]
           (let [next-move (apply move-from-slope slope)]
             (loop [[x y] [0 0]
                    tree-count 0]
               (if (<= y height)
                 (recur (next-move x y)
                        (if (has-tree? x y)
                          (inc tree-count)
                          tree-count))
                 tree-count))))))
