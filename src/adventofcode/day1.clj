(ns adventofcode.day1
  (:require [clojure.string :as string]
            [clojure.edn :refer [read-string]]
            [clojure.repl :refer :all])
  (:import [Integer]))

(def input (slurp "src/adventofcode/day1.input"))

(def expense-report (map read-string (string/split input #"\n")))

(def target 2020)

(def sums
  ;; seq of [sum x y]
  (for [x expense-report
        y expense-report]
    [(+ x y) x y]))

(defn matches?
  [[sum & args]]
  (when (= sum 2020)
    args))

(def part-1
  (let [[x y] (some matches?
                    sums)]
    (* x y)))

(println part-1)

(def sums2 (for [x expense-report
                 y expense-report
                 z expense-report]
             [(+ x y z) x y z]))

(def part-2 (let [[x y z] (some matches? sums2)]
              (* x y z)))

(println part-2)
