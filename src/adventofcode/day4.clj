(ns adventofcode.day4
  (:require [clojure.string :as string]
            [clojure.set :refer [subset?]]))

(def input (slurp "src/adventofcode/day4.input"))

(def passport-lines
  (loop [lines (string/split input #"\n")
         {:keys [passports current]} {:current () :passports ()}]
    (if-let [line (first lines)]
      (recur (rest lines)
             (if (= line "")
               {:current ()
                :passports (conj passports current)}
               {:current (conj current line)
                :passports passports}))
      (conj passports current))))

(defn parse-line
  [line]
  (reduce
    (fn [passport [key-v val-v]]
      (assoc passport
             (first key-v) (first val-v)))
    {}
    (partition 2 (re-seq #"(\w+)" line))))

(defn parse-passport
  [passport-lines]
  (reduce
    (fn [passport line]
      (merge passport
             (parse-line line)))
    {}
    passport-lines))

(def passports (map parse-passport passport-lines))

(def expected-fields #{"byr" "iyr" "eyr" "hgt" "hcl" "ecl" "pid"})

;; count valid passports
(def part-1
  (reduce
    (fn [valid-count passport]
      (if (subset? expected-fields (set (keys passport)))
        (inc valid-count)
        valid-count))
    0
    passports))
