(ns adventofcode.day2
  (:require [clojure.string :as string]))

(def input (string/split (slurp "src/adventofcode/day2.input") #"\n"))

(def regexp #"(\d+)-(\d+) (\w): (\w+)")

(def specs 
  (map #(let [[_ m M letter password] (re-matches regexp %)]
          {:min (read-string m)
           :max (read-string M)
           :letter letter
           :password password})
       input))

(def part-1
  (count
    (filter
      #(last %)
      (map 
        #(let [spec %
               pattern (re-pattern (:letter spec))
               matches (re-seq pattern (:password spec))
               occurrences (count matches)
               valid? (and (>= occurrences (:min spec))
                           (<= occurrences (:max spec)))]
           [pattern (:password spec) matches (count matches) valid?])
        specs))))


(def part-2
  (count
    (filter (partial = 1)
            (map
              #(let [spec %
                     char-vec (apply vector (:password spec))
                     password-length (count char-vec)
                     mandatory-char (first (seq (:letter spec)))
                     occurrences (+ (if (= mandatory-char (get char-vec (-> spec :min dec)))
                                      1 0)
                                    (if (= mandatory-char (get char-vec (-> spec :max dec)))
                                      1 0))]
                 occurrences)
              specs))))
